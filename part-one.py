#!/usr/bin/env python3

import os, math, operator, sys

class State:
    def __init__(self, minute, robots, resources):
        self.minute = minute
        self.robots = robots
        self.resources = resources
    
    def __repr__(self):
        return f"@{self.minute} {self.robots} {self.resources}"


def tuple_add(t1,t2):
    return tuple(map(operator.add, t1, t2))

def tuple_subtract(t1,t2):
    return tuple(map(operator.sub, t1, t2))

def tuple_multiply(t1, m):
    return tuple(map(lambda v: m*v, t1))

def tuple_divide(t1,t2):
    return max(map(lambda x,y: math.ceil(x/y) if x != 0 else 0, t1, t2))

def get_ruleset(line):
    ruleset = []
    data = line.split(":")[1].strip(" .")
    elems = data.split(". ")
    items = {
        elem.split(" robot costs ")[0].split(" ")[1]: {resources.split(" ")[1]: int(resources.split(" ")[0]) for resources in elem.split(" robot costs ")[1].split(" and ")} for elem in elems
    }
    lookup = ["ore", "clay", "obsidian", "geode"]
    for robot in lookup:
        ruleset.append(tuple(items[robot].get(key, 0) for key in lookup))
    return ruleset


def answer(input_file):
    def next_make(robot_type):
        nonlocal states, skip_count
        if s.minute == 23 and robot_type < 3:
            return
        additional_resources_needed = tuple_subtract(rules[robot_type], s.resources)
        if additional_resources_needed[0] > 0 and s.robots[0] == 0:
            return
        if additional_resources_needed[1] > 0 and s.robots[1] == 0:
            return
        if additional_resources_needed[2] > 0 and s.robots[2] == 0:
            return
        if additional_resources_needed[3] > 0 and s.robots[3] == 0:
            return
        minutes = max(tuple_divide(additional_resources_needed, s.robots), 0)
        if (minutes + s.minute + 1) >= 24:
            return
        state = State(s.minute + minutes + 1, tuple_add(s.robots, robot_build[robot_type]), tuple_subtract(tuple_add(s.resources, tuple_multiply(s.robots, minutes + 1)),rules[robot_type]))
        if robot_type < 3:
            pot_max_geodes = state.resources[3] + ((24-state.minute) * (state.robots[3] + 1))
            if state.minute >= 22 and max_geodes >= 1 and pot_max_geodes < max_geodes:
                return
            if state.minute >= 21 and max_geodes >= 3 and pot_max_geodes < max_geodes:
                return
            if state.minute >= 20 and max_geodes >= 5 and pot_max_geodes < max_geodes:
                return
            if state.minute >= 19 and max_geodes >= 7 and pot_max_geodes < max_geodes:
                return
            if state.minute >= 18 and max_geodes >= 9 and pot_max_geodes < max_geodes:
                return
        states.append(state)

    with open(input_file, "r") as input_data:
        data = input_data.read().split("\n")

    rulesets = [get_ruleset(line) for line in data]

    robot_build = [
        (1,0,0,0),
        (0,1,0,0),
        (0,0,1,0),
        (0,0,0,1)
    ]

    quality_sum = 0

    for rx in range(len(rulesets)):
        skip_count = 0
        rules = rulesets[rx]
        max_required_of_type = [max(v) for v in list(zip(*[rules[ix][0:ix]+(0,)+rules[ix][ix+1:] for ix in range(4)]))][:-1]+[sys.maxsize]
        robots = (1,0,0,0)
        resources = (0,0,0,0)
        max_geodes = 0

        states = [State(0, robots, resources)]
        while len(states) > 0:
            s = states.pop()
            for ix in range(4):
                if s.robots[ix] < max_required_of_type[ix]:
                    next_make(ix)
            final_total = s.robots[3] * (24 - s.minute) + s.resources[3]
            if final_total > max_geodes:
                max_geodes = final_total
        print(f"Blueprint {rx+1} - Max Geodes: {max_geodes} - Quality Factor: {(rx+1) * max_geodes} {skip_count}")
        quality_sum += ((rx+1) * max_geodes)

    answer = quality_sum

    print(f"The answer is *** {answer} ***")

input_file = os.path.join(os.path.dirname(__file__), "input")
answer(input_file)
